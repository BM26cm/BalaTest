

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<malloc.h>
#include<unistd.h>
#include<memory.h>
#include<arpa/inet.h>
#include<netdb.h>
#include<ctype.h>

#define BSIZE 1024
#define KVSIZE 511
#define IP "127.0.0.1"
#define PORT "1111"

void menu(){
	printf("\nMenu: \n");
	printf("1. Add Data\n");
	printf("2. Get Data\n");
	printf("3. Exit\n\n");
}


int connectToServer(const char *name, const char *port){
	struct addrinfo hints, *info;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family=AF_UNSPEC;
	hints.ai_socktype=SOCK_STREAM;
	getaddrinfo(name, port, &hints, &info);
	int fd = socket(info->ai_family, info->ai_socktype, info->ai_protocol);
	
	connect(fd, info->ai_addr, info->ai_addrlen);
	
	freeaddrinfo(info);
	return fd;
	
}
void sendMSG(char *buffer, int fd){
	int len=strlen(buffer);
	buffer[len++]='\r';
	buffer[len++]='\n';
	buffer[len]='\0';
	send(fd, buffer, len, 0);
}

void addData(int fd){
	char *buffer=(char*)malloc(KVSIZE*sizeof(char));
	char *buffer2=(char*)malloc(KVSIZE*sizeof(char));
	printf("Add key: \n");
	scanf("%s", buffer);
	printf("Add value: \n");
	scanf("%s", buffer2);
	char *b=buffer2;
	char c;
	int a=0;
	while((c = *buffer2)&&(a==0)){//check value only number and letter
		if( (isalpha(c) || isalnum(c) )){
			++buffer2;
		
		}else{ a++;}
	}
	if(a==0){
		strcat(buffer , "$");
		strcat(buffer, b);
		sendMSG(buffer, fd );
		
	}else{
		printf("Value wrong\n");
	}
	
	free(buffer);
}

int main(){
    char *bufferIn=(char*)malloc(BSIZE*sizeof(char));
	char *bufferOut=(char*)malloc(BSIZE*sizeof(char));
	
	int fd = connectToServer(IP, PORT);
	int exit = 0;
	while(exit==0){
		menu();
		fd_set fds;
		FD_ZERO(&fds);
		FD_SET(0, &fds);
		FD_SET(fd, &fds);
		select(fd+1, &fds,0,0,0);
		
		if(FD_ISSET(0, &fds)){//SEND
			int asd;
			scanf("%d", &asd);
			if(asd==3){//EXIT
				exit=1;
			}else if(asd==2){//GET DATA
				char vWanted[512];
				sprintf(bufferOut, "%d", asd);
				printf("Add wanted value: \n");
				scanf("%s", vWanted);
				strcat(bufferOut, "$");
				strcat(bufferOut, vWanted);
				sendMSG(bufferOut, fd );
			}else if(asd==1){//ADD DATA
				addData(fd);
			}else{
				printf("Input not correct!\n");
				printf("Add number!\n");
			}
		}if(FD_ISSET(fd, &fds)){//RECIVE
			
			int count= recv(fd, bufferIn, BSIZE, 0);
			if(count==0){//server connection lost
				fd=-1;
				exit=1;
			}else{//RECIVE
				bufferIn[count++]='\0';
				printf("Wanted Value: \n%s\n", bufferIn);
			}
		}
	}
	close(fd);
	free(bufferIn);
	free(bufferOut);
	return 0;
}
