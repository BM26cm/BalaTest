#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>
#include <memory.h>
#include <time.h>
#include <sys/time.h>

#define BSIZE 1024//buffersze
#define KVSIZE 512 //key value size

////////////////////////Container////////////////////
struct Container{
	struct Container *next;
	struct Container *prev;
	//int id;
	char *key;
	char *value;
};

void addData(struct Container *head, char *buffer){
	char *newKeyA=(char*)malloc(KVSIZE*sizeof(char));
	char *newValueA=(char*)malloc(KVSIZE*sizeof(char));
	char delim[]="$";
	newKeyA= strtok(buffer, delim);
	newValueA = strtok(NULL, delim); 
	printf("Adding Data...\n");
	
	struct Container *nod=(struct Container*)malloc(sizeof(struct Container));
	nod=head->next;
	int van=0;
	while(nod!=head && van==0){
		if(strcmp(newKeyA, nod->key)==0){
			strcpy(nod->value, newValueA);
			van=1;
		}
		nod=nod->next;
	}
	if(van==0){
		struct Container *newData=(struct Container*)malloc(sizeof(struct Container));
		newData->next=head->next;
		newData->prev=head;
		newData->next->prev=newData;
		newData->prev->next=newData;
		newData->key=(char*)malloc(KVSIZE*sizeof(char));
		strcpy(newData->key, newKeyA);
		newData->value=(char*)malloc(KVSIZE*sizeof(char));
		strcpy(newData->value, newValueA);
		//newData->id=head->id++;
	}
	
	printf("Data added\n");
	
	
}

void removeData(struct Container *clienta){//Client exit
	printf("Removing clinet...\n");
	clienta->prev->next=clienta->next;
	clienta->next->prev=clienta->prev;
	free(clienta->key);
	free(clienta->value);
	free(clienta);
	printf("Client removed\n");
	
}
void freeContainer(struct Container *head){
	printf("Server shoting down...\n");
	while(head->next!=head){
		removeData(head->next);
	}
	free(head->key);
	free(head->value);
	free(head);
	printf("Server shoted down!\n");
	
}
void getData(struct Container *firstnod, int fd, char *bufferIn){
	char *buffer=(char*)malloc(BSIZE*sizeof(char));
	char *wantedK=(char*)malloc(KVSIZE*sizeof(char));
	struct Container *nod=(struct Container*)malloc(sizeof(struct Container));
	char delim[]="$";
	wantedK= strtok(bufferIn, delim);
	wantedK = strtok(NULL, delim); 
	
	nod=firstnod->next;
	int megvan=0;
	while(nod!=firstnod && megvan==0){
		if(strcmp(wantedK, nod->key)){
			strcat(buffer, "Key: ");
			strcat(buffer, nod->key);
			strcat(buffer, " value: ");
			strcat(buffer, nod->value);
			strcat(buffer, "\n");
			send(fd, buffer, strlen(buffer),0);
			megvan=1;
			strcpy(buffer, "");
		}
			nod=nod->next;
	}
	if(megvan==0){
		strcpy(buffer, "No value for the key\n");
		send(fd, buffer, strlen(buffer),0);
	}
}
void printfData(struct Container*firstnod){
	struct Container *nod=(struct Container*)malloc(sizeof(struct Container));
	nod=firstnod->next;
	while(nod!=firstnod){
		printf("key: %s", nod->key);
		printf(" value: %s\n", nod->value);
		nod=nod->next;
	}
}
///////////////////////////////////Client Container//////////////
struct Client{
	int fd;
	int count;
	struct Client *head;
	struct Client *next;
	struct Client *prev;
};

struct Client* createHead(const char* port){
	struct Client *head = (struct Client*)malloc(sizeof(struct Client));
	head->head=head;
	head->next=head;
	head->prev=head;
	head->count=0;
	
	struct addrinfo hints, *info;
	int fd;
	memset(&hints, 0, sizeof(hints));
	hints.ai_family=AF_UNSPEC;
	hints.ai_socktype=SOCK_STREAM;
	hints.ai_flags=AI_PASSIVE;
	getaddrinfo( 0, port, &hints, &info);
	fd=socket(info->ai_family, info->ai_socktype, info->ai_protocol);
	int yes=1;
	setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &yes, sizeof(int));
	bind(fd, info->ai_addr, info->ai_addrlen);
	listen(fd, 10);
	
	freeaddrinfo(info);
	
	head->fd=fd;
	return head;
}

void addClient(struct Client *head){
	printf("Adding client...\n");
	struct Client *newClient=(struct Client*)malloc(sizeof(struct Client));
	newClient->head=head;
	newClient->next=head->next;
	newClient->prev=head;
	newClient->next->prev=newClient;
	newClient->prev->next=newClient;
	newClient->fd=accept(head->fd,0,0);
	newClient->count=0;
	head->count++;
	printf("Client added\n");
	
}

void removeClient(struct Client *clienta){//Client exit
	printf("Removing clinet...\n");
	close(clienta->fd);
	clienta->prev->next=clienta->next;
	clienta->next->prev=clienta->prev;
	clienta->head->count--;
	free(clienta);
	printf("Client removed\n");
	
}
////////////////////////////////////////////////////
void serverShotDown(struct Client *head){
	printf("Server shoting down...\n");
	close(head->fd);
	while(head->next!=head){
		removeClient(head->next);
	}
	free(head);
	printf("Server shoted down!\n");
	
}

void collectClients(fd_set *fds, struct Client*head){
	printf("cheking clients...\n");
	FD_ZERO(fds);
	FD_SET(head->fd, fds);
	int fdmax = head->fd;
	struct Client *current = head->next;
	while(current!=head){
		if(fdmax<current->fd){
			fdmax=current->fd;
		}
		FD_SET(current->fd, fds);
		current=current->next;
	}
	select(fdmax+1, fds, 0, 0 ,0 );
	printf("Clients checked\n");
}
int main(){
	
	struct Client *head=createHead("1111");
	struct Client *current;
	char *bufferIn=(char*)malloc(BSIZE*sizeof(char));
	char *bufferOut=(char*)malloc(BSIZE*sizeof(char));
	fd_set fd;
	
	struct Container *dataStore=(struct Container*)malloc(sizeof(struct Container));
	dataStore->next=dataStore;
	dataStore->prev=dataStore;
	dataStore->key="";
	dataStore->value="";
	//dataStore->id=0;
	
	while(1){
		collectClients(&fd, head);
		if(FD_ISSET(head->fd, &fd)){
			addClient(head);
		}
		current=head->next;
		while(current!=head){
			int count=-1;
			if(FD_ISSET(current->fd, &fd)){
				count=recv(current->fd, bufferIn, BSIZE, 0);
				if(count>0){
					if(bufferIn[count-1]!='\0') bufferIn[count-1]='\0';//záro karakter elhejezése hiany eseten
						int asd = atoi(bufferIn);
						if(asd==2){//getData
							printf("serving Data...\n");
							getData(dataStore, current->fd, bufferIn);
							printf("Data served\n");
						}else{//addData
							addData(dataStore, bufferIn);
							printfData(dataStore);
						}
				}
			}
			current=current->next;
			if(count==0){//client exit
				removeClient(current->prev);
			}
		}
		
	}
	
	freeContainer(dataStore);
	serverShotDown(head);
	free(bufferIn);
	free(bufferOut);
	
	
	
	return 0;
}